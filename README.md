Project setup

1 - download project from https://bitbucket.org/aweichandt/note-test

2 - install npm packages (run "npm install" in project root dir)

Running iOS

* From command line
1 - run npm install -g react-native-cli

2 - from project root dir run "react-native run-ios"

* From xcode
1 - open /ios/casumo.xcodeproj

2 - build and run on ios simulator pressing play button

Running iOS

* From command line
1 - run npm install -g react-native-cli

2 - from project root dir run "react-native run-android"
