import React from 'react';
import PropTypes from 'prop-types';
import { addNavigationHelpers } from 'react-navigation'; // 1.0.0-beta.26

import AppNavigator from '../../common/navigation';

const App = ({ dispatch, state }) => (
  <AppNavigator
    navigation={addNavigationHelpers({
      dispatch,
      state,
    })}
  />
);
App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  state: PropTypes.shape({
  }).isRequired,
};
export default App;
