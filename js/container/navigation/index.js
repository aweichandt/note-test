import { connect } from 'react-redux'; // 5.0.6
import App from './view';

const mapStateToProps = ({ navState }) => ({
  state: navState,
});

export default connect(mapStateToProps)(App);
