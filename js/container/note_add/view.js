import React from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31B2AA',
  },
  input: {
    flex: 1,
    alignSelf: 'stretch',
    color: '#EAF4F4',
  },
});

export default class NotesAddView extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }
  componentDidMount() {
    const { submit, navigation } = this.props;
    navigation.setParams({ submit });
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          multiline
          placeholder={"what's on your mind"}
          placeholderTextColor="#BAC4C4"
          onChangeText={(text) => {
            this.setState({ text });
            this.props.navigation.setParams({ text });
          }}
          value={this.state.text}
          onSubmitEditing={() => this.props.submit(this.state.text)}
        />
      </View>
    );
  }
}
NotesAddView.propTypes = {
  submit: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    setParams: PropTypes.func,
  }).isRequired,
};
