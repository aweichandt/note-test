import { connect } from 'react-redux'; // 5.0.6
import NoteAddView from './view';
import { submitNote } from '../../common/actions';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  submit: text => dispatch(submitNote(text)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NoteAddView);
