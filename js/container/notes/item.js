import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  Animated,
} from 'react-native';

import { OpenState } from '../../common/reducer/notes';

import RowSideButton, { createLeftAnim, createRightAnim } from '../../component/rowSideButton';
import SwipeableRow from '../../component/swipeableRow';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'stretch',
  },
  buttons: {
    flexDirection: 'row',
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
  },
  swpieable: {
    flex: 1,
  },
  note: {
    flex: 1,
    margin: 10,
    padding: 10,
    borderRadius: 5,
    alignItems: 'center',
    backgroundColor: '#EAF4F4',
  },
  noteText: {
    color: 'black',
    textAlign: 'left',
  },
});

const sideMapping = (menuState) => {
  switch (menuState) {
    case OpenState.OPEN_FAVOURITE:
      return 1;
    case OpenState.OPEN_REMOVE:
      return -1;
    default:
      return 0;
  }
};

class Note extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      swipeableAnimProps: {},
      leftButtonProps: {},
      rightButtonProps: {},
    };
    this.setupTransforms = this.setupTransforms.bind(this);
  }
  setupTransforms(offset, limit) {
    const leftButtonProps = createLeftAnim(offset, limit);
    const rightButtonProps = createRightAnim(offset, limit);
    const swipeableAnimProps = {
      transform: [{
        scale: offset.interpolate({
          inputRange: [-limit, 0],
          outputRange: [0.8, 1],
          extrapolate: 'clamp',
        }),
      }],
      opacity: offset.interpolate({
        inputRange: [-limit, 0],
        outputRange: [0.4, 1],
        extrapolate: 'clamp',
      }),
    };
    this.setState({ swipeableAnimProps, leftButtonProps, rightButtonProps });
  }
  render() {
    const {
      swipeableAnimProps, leftButtonProps, rightButtonProps,
    } = this.state;
    const {
      menuState, text, favourite, toggle, remove,
    } = this.props;
    const swipeProps = {
      ...this.props,
      hasLeft: true,
      hasRight: true,
      side: sideMapping(menuState),
      transforms: [this.setupTransforms],
    };
    return (
      <View style={[styles.container]}>
        <View style={[styles.buttons]}>
          <RowSideButton
            animStyles={leftButtonProps}
            icon={favourite ? 'heart' : 'heartOutline'}
            onPress={toggle}
          />
          <View style={{ flex: 1 }} />
          <RowSideButton
            animStyles={rightButtonProps}
            icon="trash"
            onPress={remove}
          />
        </View>
        <SwipeableRow {...swipeProps} >
          <Animated.View style={[styles.swpieable, swipeableAnimProps]}>
            <View style={styles.note}>
              <Text style={styles.noteText}>
                {text}
              </Text>
            </View>
          </Animated.View>
        </SwipeableRow>
      </View>
    );
  }
}

Note.propTypes = {
  menuState: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  favourite: PropTypes.bool,
  remove: PropTypes.func,
  toggle: PropTypes.func,
};
Note.defaultProps = {
  favourite: false,
  remove: () => {},
  toggle: () => {},
};

export default Note;
