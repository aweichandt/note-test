import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  FlatList,
  StyleSheet,
} from 'react-native';

import Note from './item';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#31B2AA',
  },
});

export default class NotesView extends React.PureComponent {
  constructor(props) {
    super(props);
    this.toggleFilter = this.toggleFilter.bind(this);
  }
  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({ toggle: this.toggleFilter });
  }
  componentWillReceiveProps({ filterEnabled }) {
    if (filterEnabled !== this.props.filterEnabled) {
      this.props.navigation.setParams({ toggleEnabled: filterEnabled });
    }
  }
  toggleFilter() {
    const { toggleFavouriteFilter, filterEnabled } = this.props;
    toggleFavouriteFilter(filterEnabled);
  }
  render() {
    const {
      items, filterEnabled, removeItem, toggleFavourite,
      openFavouriteMenu, openRemoveMenu, closeMenu,
    } = this.props;
    const filteredItems = items.filter(item => (filterEnabled ? item.favourite : true));
    const itemProps = item => ({
      ...item,
      onReachLeft: () => openRemoveMenu(item),
      onReachRight: () => openFavouriteMenu(item),
      onBack: () => closeMenu(item),
      remove: () => removeItem(item),
      toggle: () => toggleFavourite(item),
    });
    return (
      <View style={styles.container}>
        <FlatList
          data={filteredItems}
          keyExtractor={({ id }) => id}
          renderItem={({ item }) => (
            <Note {...itemProps(item)} />
          )}
        />
      </View>
    );
  }
}

NotesView.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    favourite: PropTypes.bool,
  })),
  filterEnabled: PropTypes.bool,
  removeItem: PropTypes.func,
  openFavouriteMenu: PropTypes.func,
  openRemoveMenu: PropTypes.func,
  closeMenu: PropTypes.func,
  toggleFavourite: PropTypes.func,
  toggleFavouriteFilter: PropTypes.func,
  navigation: PropTypes.shape({
    setParams: PropTypes.func,
  }).isRequired,
};
NotesView.defaultProps = {
  items: [],
  filterEnabled: false,
  removeItem: () => {},
  openFavouriteMenu: () => {},
  openRemoveMenu: () => {},
  closeMenu: () => {},
  toggleFavourite: () => {},
  toggleFavouriteFilter: () => {},
};
