import { connect } from 'react-redux'; // 5.0.6
import NotesView from './view';
import {
  setNotesFavouriteFilter,
  setFavouriteNote,
  removeNote,
  openNoteMenu,
  closeNoteMenu,
} from '../../common/actions';
import { OpenState } from '../../common/reducer/notes';

const mapStateToProps = state => ({
  items: state.notes,
  filterEnabled: state.favouriteFilter,
});

const mapDispatchToProps = dispatch => ({
  removeItem: ({ id }) => dispatch(removeNote(id)),
  openFavouriteMenu: ({ id }) => dispatch(openNoteMenu(id, OpenState.OPEN_FAVOURITE)),
  openRemoveMenu: ({ id }) => dispatch(openNoteMenu(id, OpenState.OPEN_REMOVE)),
  closeMenu: ({ id }) => dispatch(closeNoteMenu(id)),
  toggleFavourite: ({ id, favourite }) =>
    dispatch(setFavouriteNote(id, !favourite)),
  toggleFavouriteFilter: value => dispatch(setNotesFavouriteFilter(!value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NotesView);
