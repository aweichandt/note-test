import React from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  PanResponder,
} from 'react-native';
/* eslint-disable import/no-extraneous-dependencies */
import Dimensions from 'Dimensions';
/* eslint-enable nimport/o-extraneous-dependencies */

const { width } = Dimensions.get('window');

const createPanResponder = component =>
  PanResponder.create({
    onPanResponderGrant: component.shouldGrant,
    onMoveShouldSetPanResponder: () => true,
    onPanResponderMove: component.onMove,
    onPanResponderEnd: component.onEndMovement,
    onPanResponderTerminate: component.onEndMovement,
  });

export default class SwipeableRow extends React.PureComponent {
  constructor(props) {
    super(props);
    const dx = new Animated.Value(0);
    const limit = props.parentWidth / 3;
    if (props.transforms) {
      this.props.transforms.forEach(transform => transform(dx, limit));
    }
    this.state = {
      dx,
      limit,
      animate: false,
      offset: 0,
    };
    this.shouldGrant = this.shouldGrant.bind(this);
    this.onMove = this.onMove.bind(this);
    this.onEndMovement = this.onEndMovement.bind(this);
    this.onEndAnimation = this.onEndAnimation.bind(this);
    this._panResponder = createPanResponder(this);
  }
  componentWillReceiveProps({ side }) {
    if (side !== this.props.side) {
      const offset = this.state.limit * side;
      this.onStartAnimation(offset);
    }
  }
  shouldGrant() {
    return !this.state.animate;
  }
  onMove(event, gestureState) {
    const mergedGesture = {
      ...gestureState,
      dx: gestureState.dx + this.state.offset,
    };
    return Animated.event([null, { dx: this.state.dx }])(event, mergedGesture);
  }
  onEndMovement(event, { dx }) {
    const {
      hasLeft, hasRight, onReachLeft, onReachRight, onBack,
    } = this.props;
    const { limit } = this.state;
    const allDx = dx + this.state.offset;
    const midDistance = Math.abs(allDx);
    const leftDistance = hasLeft ? Math.abs(allDx + limit) : Number.MAX_VALUE;
    const rightDistance = hasRight ? Math.abs(allDx - limit) : Number.MAX_VALUE;
    const minDistance = Math.min(midDistance, leftDistance, rightDistance);
    let offset = 0;
    offset = minDistance === leftDistance ? -limit : offset;
    offset = minDistance === rightDistance ? limit : offset;
    this.onStartAnimation(offset);
    const isLeft = offset < 0;
    const isRight = offset > 0;
    if (hasLeft && isLeft) {
      onReachLeft();
    } else if (hasRight && isRight) {
      onReachRight();
    } else {
      onBack();
    }
  }
  onStartAnimation(offset) {
    this.setState({ offset, animate: true });
    Animated.spring(this.state.dx, {
      toValue: offset,
    }).start(this.onEndAnimation);
  }
  onEndAnimation() {
    this.setState({ animate: false });
  }
  render() {
    return (
      <Animated.View
        {...this._panResponder.panHandlers}
        style={{ flex: 1, alignSelf: 'stretch', transform: [{ translateX: this.state.dx }] }}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

SwipeableRow.propTypes = {
  hasLeft: PropTypes.bool,
  hasRight: PropTypes.bool,
  side: PropTypes.number.isRequired,
  parentWidth: PropTypes.number,
  transforms: PropTypes.arrayOf(PropTypes.func),
  onReachLeft: PropTypes.func,
  onReachRight: PropTypes.func,
  onBack: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]),
};
SwipeableRow.defaultProps = {
  hasLeft: false,
  hasRight: false,
  parentWidth: width,
  transforms: [],
  onReachLeft: () => {},
  onReachRight: () => {},
  onBack: () => {},
  children: [],
};
