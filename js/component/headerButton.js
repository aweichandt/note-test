import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, Image, StyleSheet } from 'react-native';

import resources from '../common/resources';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
  },
  icon: {
    flex: 1,
    tintColor: '#EAF4F4',
  },
  text: {
    color: '#EAF4F4',
    fontSize: 15,
  },
});

const Button = ({
  dispatch, action, callback, text, icon,
}) => {
  const onPress = () => {
    if (action) {
      dispatch(action);
    }
    if (callback) {
      callback();
    }
  };
  return (
    <TouchableOpacity
      style={styles.button}
      onPress={onPress}
    >
      {!text ? undefined : <Text style={styles.text}>{text}</Text>}
      {!icon ? undefined :
      <Image source={resources[icon]} style={styles.icon} resizeMode="contain" />}
    </TouchableOpacity>
  );
};
Button.propTypes = {
  dispatch: PropTypes.func,
  action: PropTypes.shape({
    type: PropTypes.string.isRequired,
  }),
  callback: PropTypes.func,
  text: PropTypes.string,
  icon: PropTypes.string,
};
Button.defaultProps = {
  dispatch: undefined,
  action: undefined,
  callback: undefined,
  text: undefined,
  icon: undefined,
};

export default Button;
