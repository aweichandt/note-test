import React from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  Animated,
} from 'react-native';

import resources from '../common/resources';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    flex: 1,
    tintColor: '#EAF4F4',
  },
  text: {
    textAlign: 'center',
    color: 'black',
  },
});

export const createLeftAnim = (offset, limit) => ({
  opacity: offset.interpolate({
    inputRange: [0, limit],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  }),
});
export const createRightAnim = (offset, limit) => ({
  opacity: offset.interpolate({
    inputRange: [-limit, 0],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  }),
});

const RowSideButton = ({
  icon, text, onPress, animStyles,
}) => (
  <Animated.View style={[styles.button, animStyles]}>
    <TouchableOpacity style={styles.button} onPress={onPress}>
      {!text ? undefined : <Text style={styles.text}>{text}</Text>}
      {!icon ? undefined :
      <Image source={resources[icon]} style={styles.icon} resizeMode="contain" />}
    </TouchableOpacity>
  </Animated.View>
);

/* eslint-disable react/forbid-prop-types */
RowSideButton.propTypes = {
  onPress: PropTypes.func,
  text: PropTypes.string,
  icon: PropTypes.string,
  animStyles: PropTypes.any,
};
/* eslint-enable react/forbid-prop-types */
RowSideButton.defaultProps = {
  onPress: () => {},
  text: undefined,
  icon: undefined,
  animStyles: {},
};

export default RowSideButton;
