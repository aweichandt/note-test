import React from 'react';
import 'redux'; // 3.7.2
import { Provider } from 'react-redux'; // 5.0.6

import configureStore from './common/store';
import Navigator from './container/navigation';

const testData = {
  notes: [
    {
      id: 5,
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et ligula posuere, finibus neque sit amet, consectetur ex. Nam vel iaculis est. Maecenas congue metus at tellus tincidunt congue id tempor lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eleifend, tellus sed facilisis rutrum, ex quam efficitur leo, ut aliquam augue odio vel lorem. Sed ac ipsum sed lacus efficitur sollicitudin ac eu urna. Integer porta orci et lectus convallis blandit. Curabitur egestas, nibh vel hendrerit sodales, purus ipsum rutrum est, id auctor est lacus sed sem. In tristique nunc eget ex facilisis, aliquam lacinia metus egestas. Vestibulum a porttitor sem.',
      favourite: false,
      menuState: 'close',
    },
    {
      id: 4,
      text: 'Vestibulum non fringilla mauris, vel lobortis quam. Fusce consectetur odio eget porttitor lobortis. Nulla tempus velit at imperdiet porta. Sed eu lorem eget lectus finibus mollis a id libero. Fusce suscipit id augue et laoreet. Aliquam erat volutpat. Integer sollicitudin lobortis fermentum.',
      favourite: true,
      menuState: 'close',
    },
    {
      id: 3,
      text: 'short test',
      favourite: false,
      menuState: 'close',
    },
    {
      id: 2,
      text: 'Mauris eget imperdiet arcu. Cras pulvinar, augue vel mollis dapibus, erat leo pellentesque mi, non vulputate mi magna id leo. Nunc non feugiat urna. In nisl lacus, rutrum vel nibh et, tristique lacinia mi. Proin sollicitudin tortor dolor, nec laoreet augue congue sit amet. Sed molestie dictum ipsum vitae fringilla. Quisque aliquam placerat luctus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur ac enim lacus. Nullam a diam urna. Curabitur aliquam aliquam mollis. Aenean at ullamcorper purus. Nunc bibendum quam ipsum, id malesuada est luctus sit amet. Suspendisse nunc sapien, accumsan et velit non, placerat finibus metus.',
      favourite: false,
      menuState: 'close',
    },
    {
      id: 1,
      text: 'Sed eu velit velit. Aenean non ipsum vitae lectus finibus imperdiet. Duis ultrices eros eget metus vulputate laoreet. Donec vestibulum ac odio sit amet mollis. Nunc ultricies rhoncus dolor ut feugiat. Nullam porta felis nec dui ornare lacinia. Quisque nulla lorem, iaculis eget velit id, dictum consectetur velit.',
      favourite: false,
      menuState: 'close',
    },
    {
      id: 0,
      text: 'Vivamus eu dictum arcu, eu dapibus massa. Nam placerat posuere augue, vel tempus tellus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam convallis nunc vel nisl mattis, id condimentum elit viverra. Suspendisse condimentum risus et odio fringilla, quis luctus sem commodo. Quisque rutrum leo est, id vehicula odio sagittis at. Praesent laoreet odio eget metus finibus tempor. Etiam sem nibh, efficitur laoreet vestibulum maximus, congue vel tellus.',
      favourite: true,
      menuState: 'close',
    },
  ],
};

const store = configureStore(testData);
export default () => (
  <Provider store={store}>
    <Navigator />
  </Provider>
);
