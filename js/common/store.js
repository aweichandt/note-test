import { createStore, applyMiddleware } from 'redux'; // 3.7.2
import thunk from 'redux-thunk'; // 2.2.0

import rootReducer from './reducer';

export default state => createStore(rootReducer, state, applyMiddleware(thunk));
