import addIcon from '../../res/add.png';
import deleteIcon from '../../res/delete.png';
import checkIcon from '../../res/check.png';
import heartIcon from '../../res/heart.png';
import heartOutlineIcon from '../../res/heart-outline.png';
import TrashIcon from '../../res/trash.png';

export default {
  add: addIcon,
  delete: deleteIcon,
  check: checkIcon,
  heart: heartIcon,
  heartOutline: heartOutlineIcon,
  trash: TrashIcon,
};
