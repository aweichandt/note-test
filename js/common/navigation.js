import React from 'react';
import { StackNavigator } from 'react-navigation'; // 1.0.0-beta.26

import Button from '../component/headerButton';

import NotesView from '../container/notes';
import NoteAddView from '../container/note_add';

import { pushScreen, popScreen } from './actions';

const AppRouteConfigs = {
  notes: {
    screen: NotesView,
    navigationOptions: ({ navigation }) => {
      const { params = {} } = navigation.state;
      const { toggleEnabled = false, toggle = () => {} } = params;
      const icon = toggleEnabled ? 'heart' : 'heartOutline';
      return {
        title: 'Notes',
        headerLeft: <Button icon={icon} callback={toggle} />,
        headerRight: <Button {...navigation} icon="add" action={pushScreen('note_add')} />,
      };
    },
  },
  note_add: {
    screen: NoteAddView,
    navigationOptions: ({ navigation }) => {
      const { params = {} } = navigation.state;
      const { submit = () => {}, text = '' } = params;
      return {
        title: 'Add Note',
        headerLeft: <Button icon="check" callback={() => submit(text)} />,
        headerRight: <Button {...navigation} icon="delete" action={popScreen()} />,
      };
    },
  },
};

const AppHeaderConfig = {
  headerMode: 'screen',
  navigationOptions: {
    headerStyle: { backgroundColor: '#267F7A' },
    headerTitleStyle: { color: '#EAF4F4' },
  },
};

export default StackNavigator(AppRouteConfigs, AppHeaderConfig);
