import { NavigationActions } from 'react-navigation'; // 1.0.0-beta.26

export const names = {
  NOTE_ADD: 'NOTE_ADD',
  NOTE_REMOVE: 'NOTE_REMOVE',
  NOTE_OPEM_MENU: 'NOTE_OPEN_MENU',
  NOTE_CLOSE_MENU: 'NOTE_CLOSE_MENU',
  NOTE_SET_FAVOURITE: 'NOTE_SET_FAVOURITE',
  NOTE_SET_FAVOURITE_FILTER: 'NOTE_SET_FAVOURITE_FILTER',
};

export const pushScreen = id => NavigationActions.navigate({ routeName: id });

export const popScreen = () => NavigationActions.back();

export const addNote = text => ({
  type: names.NOTE_ADD,
  text,
});

export const removeNote = id => ({
  type: names.NOTE_REMOVE,
  id,
});

export const openNoteMenu = (id, side) => ({
  type: names.NOTE_OPEM_MENU,
  id,
  side,
});

export const closeNoteMenu = id => ({
  type: names.NOTE_CLOSE_MENU,
  id,
});

export const setFavouriteNote = (id, value) => ({
  type: names.NOTE_SET_FAVOURITE,
  id,
  value,
});

export const setNotesFavouriteFilter = value => ({
  type: names.NOTE_SET_FAVOURITE_FILTER,
  value,
});

export const submitNote = text => (dispatch) => {
  Promise.resolve(text)
    .then((finalText) => {
      dispatch(addNote(finalText));
      return text;
    })
    .catch(() => undefined)
    .then(() => {
      dispatch(popScreen());
      return text;
    });
};
