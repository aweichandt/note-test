import { combineReducers } from 'redux'; // 3.7.2

import notes from './notes';
import navState from './navState';
import favouriteFilter from './favouriteFilter';

export default combineReducers({
  notes,
  favouriteFilter,
  navState,
});
