import { names } from '../actions';

const {
  NOTE_SET_FAVOURITE_FILTER,
} = names;

export default (state = false, action) => {
  switch (action.type) {
    case NOTE_SET_FAVOURITE_FILTER:
      return action.value;
    default:
      return state;
  }
};
