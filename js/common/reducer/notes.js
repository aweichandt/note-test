import { names } from '../actions';

const {
  NOTE_ADD, NOTE_REMOVE, NOTE_SET_FAVOURITE, NOTE_OPEM_MENU, NOTE_CLOSE_MENU,
} = names;

export const OpenState = {
  OPEN_FAVOURITE: 'favourite',
  OPEN_REMOVE: 'remove',
  CLOSE: 'close',
};

const nextId = notes =>
  notes.reduce((max, { id }) => Math.max(id, max), -1) + 1;

export default (state = [], action) => {
  switch (action.type) {
    case NOTE_ADD:
      return [
        {
          id: nextId(state),
          text: action.text,
          favourite: false,
          menuState: OpenState.CLOSE,
        },
        ...state,
      ];
    case NOTE_REMOVE:
      return state.filter(note => note.id !== action.id);
    case NOTE_SET_FAVOURITE:
      return state.map((note) => {
        const { id, favourite } = note;
        return {
          ...note,
          favourite: id === action.id ? action.value : favourite,
        };
      });
    case NOTE_OPEM_MENU:
      return state.map((note) => {
        const { id } = note;
        return {
          ...note,
          menuState: id === action.id ? action.side : OpenState.CLOSE,
        };
      });
    case NOTE_CLOSE_MENU:
      return state.map((note) => {
        const { id, menuState } = note;
        return {
          ...note,
          menuState: id === action.id ? OpenState.CLOSE : menuState,
        };
      });
    default:
      return state;
  }
};
